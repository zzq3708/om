
/**
 * 算精準度
 */
export default class compute {
  
  constructor() {}
  // 減法計算精準度
  static accSub(num1, num2) {  
    let r1;
    let r2;
    try {
      r1 = num1.toString().split('.')[1].length;
    } catch (e) {
      r1 = 0;
    }
    try {
      r2 = num2.toString().split('.')[1].length;
    } catch (e) {
      r2 = 0;
    }
    const m = Math.pow(10, Math.max(r1, r2));
    const n = (r1 >= r2) ? r1 : r2;
    return (Math.round(num1 * m - num2 * m) / m).toFixed(n);
}

	// 乘法
   static NumberMul(arg1, arg2) {
    var m = 0;
	arg1 = arg1 + '';
	arg2 = arg2 + '';
    var s1 = arg1.toString();
    var s2 = arg2.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) {}
    try {
        m += s2.split(".")[1].length;
    } catch (e) {}
 
    return Math.floor(Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m) * 100) / 100 
	;
}
// 除法
	static floatDivide(arg1, arg2) {
		if(arg1 == null){
			return null;
		}
		if(arg2 == null || arg2 == 0){
			return null;
		}
		var n1,n2;
		var r1, r2; // 小数位数
		try {
			r1 = arg1.toString().split(".")[1].length;
		} catch (e) {
			r1 = 0;
		}
		try {
			r2 = arg2.toString().split(".")[1].length;
		} catch (e) {
			r2 = 0;
		}
		n1 = Number(arg1.toString().replace(".", ""));
		n2 = Number(arg2.toString().replace(".", ""));
		let result = (n1 / n2) * Math.pow(10, r2 - r1);
		return result.toFixed(2);
	}
	// arg1/arg2 转换为百分比
	static GetPercent(arg1, arg2) {
	   let result=  this.floatDivide(arg1, arg2);
	   return  this.NumberMul(result,100);
	}
}
