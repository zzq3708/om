package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-12
 */
public interface MemberRetailService extends IService<MemberRetail> {

    BoostrapDataGrid pageList(MemberRetailQueryObject qo);
}

