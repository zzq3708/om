package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
public interface MemberSignRuleService extends IService<MemberSignRule> {

    BoostrapDataGrid ruleList(MemberSignRuleQueryObject qo);

}

